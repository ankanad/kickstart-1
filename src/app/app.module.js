(function() {
	'use strict';
	angular.module('kickstart', [
		'md.data.table',
		'angular-loading-bar',
		'ngAnimate',
		'ngCookies',
		'ngSanitize',
		'ngMessages',
		'ngAria',
		'ui.router',
		'ngMaterial',
		'toastr',
		'restangular',
		'LocalStorageModule',
		'ui.validate',
		'ngMaterialDatePicker'
		]);
})();

angular.module('kickstart').filter('true_false', function() {
    return function(text) {
        if (text) {
            return 'Yes';
        }
        return 'No';
    }
});