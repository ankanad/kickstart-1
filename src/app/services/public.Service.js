(function() {
    angular.module('kickstart').factory('publicService', publicService);

    function publicService(Restangular) {
        var self = this;
        self.login = function(user) {
            var data = {
                mobile: user.number,
                password: user.password
            };
            return Restangular.all('users').all('login').post(data);
        };
        self.signup = function(user) {
            var data = {
                name:user.name,
                mobile: user.number,
                password: user.password
            };
            return Restangular.all('users').all('signup').post(data);
        };
        return self;
    }
})();
