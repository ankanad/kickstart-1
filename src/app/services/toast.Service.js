(function() {
    angular.module('kickstart').service('toastService', toastService);

    function toastService(toastr) {
        var self = this;
        self.show = function(msg) {
            toastr.success(msg);
        };
        self.showError = function(msg) {
            toastr.error(msg);
        };
    }
})();