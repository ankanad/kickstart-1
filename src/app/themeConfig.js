(function() {
	angular.module('kickstart').config(themingConfig);
	//theme setting
	function themingConfig($mdThemingProvider) {
		$mdThemingProvider.theme('default').primaryPalette('purple', {
				'default': '800',
				'hue-1': '100',
				'hue-2': '900'
			})
			// If you specify less than all of the keys, it will inherit from the
			// default shades
			.accentPalette('grey', {
				'default': '400' // use shade 200 for default, and keep all other shades the same
			});
 
	}
})();