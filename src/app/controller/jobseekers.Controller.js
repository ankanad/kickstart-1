(function() {
    angular.module('kickstart').controller('jobseekersCtrl', jobseekersCtrl);

    function jobseekersCtrl(appService, toastService, $scope, $state, $mdDialog) {
        var jobseekers = this;
        jobseekers.query = {
            limit: 5,
            page: 1
        };
        jobseekers.getJobseekers = function(page, limit) {
            jobseekers.query.limit = limit;
            jobseekers.query.page = page;
            appService.getJobseekers(jobseekers.query).then(function(response) {
                jobseekers.jobseekers = response;
            });
        }
        jobseekers.showDetails = function(jobseeker) {
            $mdDialog.show({
                locals: { jobseeker: jobseeker },
                clickOutsideToClose: true,
                templateUrl: 'app/views/jobseeker.html',
                controller: function($scope, jobseeker) {
                    $scope.jobseeker = jobseeker;
                }
            });
        }

        jobseekers.getJobseekers(jobseekers.query.page, jobseekers.query.limit);
        jobseekers.goToCreateJobseeker = function() {
            $state.go('main.createJobseeker');
        }
        jobseekers.goToEditJobseeker = function(event, jobseekerId) {
            event.stopPropagation();
            $state.go('main.editJobseeker', { jobseekerId: jobseekerId });
        }
        jobseekers.deleteJobseeker = function(event, jobseekerId, index) {
            event.stopPropagation();
            appService.deleteJobseeker(jobseekerId).then(function(response) {
                toastService.show('Jobseeker deleted successfully');
                jobseekers.getJobseekers(jobseekers.query.page, jobseekers.query.limit);
            });
        }

    }
})();
