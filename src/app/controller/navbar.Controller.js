(function() {
	angular.module('kickstart').controller('navbarCtrl', navbarCtrl);

	function navbarCtrl($mdSidenav, $mdUtil, userService,$rootScope) {
		var navbar = this;
		navbar.sideAvatar = userService.avatarThumb;
		$rootScope.$on('userAuthenticated', function() {
			navbar.sideAvatar=userService.avatarThumb;
		})
		$rootScope.$on('userUnauthenticated', function() {
			navbar.sideAvatar=null;
		})
		navbar.toggleLeft = function() {
				$mdSidenav('menu').toggle();
			}
			// buildToggler('menu');
		function buildToggler(navID) {
			var debounceFn = $mdUtil.debounce(function() {
				$mdSidenav(navID).toggle().then(function() {});
			}, 300);
			return debounceFn;
		}
	}
})();