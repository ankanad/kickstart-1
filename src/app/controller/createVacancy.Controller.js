(function() {
    angular.module('kickstart').controller('createVacancyCtrl', createVacancyCtrl);

    function createVacancyCtrl(appService, toastService, $scope, $state, $timeout) {
        var createVacancy = this;
        createVacancy.data = {
            interviewDates: [{ date: '' }]
                // extraFields: [{ name: '', value: '' }]
        }
        appService.getTrades().then(function(response) {
                createVacancy.allDropdowns = response.data;
            })
            // createVacancy.addField = function() {
            //     createVacancy.data.extraFields.push({ name: '', value: '' });
            // }
            // createVacancy.removeField = function(index) {
            //     createVacancy.data.extraFields.splice(index, 1);
            // }
        createVacancy.addDate = function() {
            createVacancy.data.interviewDates.push({ date: '' });
        }
        createVacancy.removeDate = function(index) {
            createVacancy.data.interviewDates.splice(index, 1);
        }
        createVacancy.create = function(callbacks) {
            appService.createVacancy(createVacancy.data).then(function(result) {
                $timeout(function() {
                    $state.go('main.vacancies');
                    toastService.show('Vacancy created');
                }, 1000);

            }).finally(function() {
                callbacks.formSubmitted();
            })
        }
        createVacancy.goToVacancies = function() {
            $state.go('main.vacancies');

        }
    }
})();
