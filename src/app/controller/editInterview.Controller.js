(function() {
    angular.module('kickstart').controller('editInterviewCtrl', editInterviewCtrl);

    function editInterviewCtrl(appService, toastService, $scope, $state, $stateParams, $timeout) {
        var editInterview = this;
        var interview = {
            id: $stateParams.interviewId
        };
        appService.getTrades().then(function(response) {
            editInterview.allDropdowns = response.data;
        })
        appService.getInterviews(interview).then(function(result) {
            editInterview.data = result.data[0];
            // editInterview.data.interviewTime = new Date(parseInt(editInterview.data.interviewTime));
            if (editInterview.data.joiningDate)
                editInterview.data.joiningDate = new Date(parseInt(editInterview.data.joiningDate));
            if (editInterview.data.leavingDate)
                editInterview.data.leavingDate = new Date(parseInt(editInterview.data.leavingDate));
        });
        appService.getJobseekers({}).then(function(response) {
            editInterview.jobseekers = response.data;
        });
        appService.getVacancies({}).then(function(response) {
            editInterview.vacancies = response.data;
        });
        editInterview.edit = function(callbacks) {
            appService.editInterview(editInterview.data, interview.id).then(function(result) {
                $timeout(function() {
                    $state.go('main.interviews');
                    toastService.show('Interview edited');
                }, 1000);

            }).finally(function() {
                callbacks.formSubmitted();
            })
        }
    }
})();
